package id.co.maminfaruq.bukuhutang;

import java.util.ArrayList;

import id.co.maminfaruq.bukuhutang.model.Note;

public interface LoadNotesCallback {
    void preExecute();

    void postExecute(ArrayList<Note> notes);
}
