package id.co.maminfaruq.bukuhutang.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DatabaseHelper extends SQLiteOpenHelper {
    private static final String DATABASE_NAME = "db_note";

    private static final int DATABASE_VERSION = 1;

    private static final String SQL_CREATE_TABLE_NOTE = String.format("CREATE TABLE %s"
                    + "(%s INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    " %s TEXT NOT NULL ," +
                    " %s TEXT NOT NULL ," +
                    " %s TEXT NOT NULL)",
            DatabaseContract.NotesColumns.TABLE_NAME,
            DatabaseContract.NotesColumns._ID,
            DatabaseContract.NotesColumns.TITLE,
            DatabaseContract.NotesColumns.DESCRIPTION,
            DatabaseContract.NotesColumns.DATE
    );

    DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_TABLE_NOTE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(" DROP TABLE IF EXISTS " + DatabaseContract.NotesColumns.TABLE_NAME);
        onCreate(db);
    }
}
